package driver.api;

import vehicle.api.Vehicle;

public class Driver {
    public Vehicle vehicle;
    public String name;
    public int age;
    public String address;
    public boolean banned = false;

    public String toString() {
        String driver = " { vehicle: " + vehicle.toString();
        driver += ", name: " + name;
        driver += ", age: " + age;
        driver += ", address: " + address;
        driver += ", banned: " + banned;
        driver += "}";
        return driver;
    }
}
