1. Open a text editor of your choice
2. Enter the following text:
```
	public class HelloWorld {
		public static void main(String[] args) {
			System.out.println("Hello World!");
		}
	}
```
3. Save the file with the name HelloWorld.java
4. Open terminal
5. Navigate to the location of the file using your terminal application
6. Type `javac HelloWorld.java` and hit enter
7. Type `ls` and hit enter - this should show a HelloWorld.class file, our java file has been compiled.
8. Type `java HelloWorld` and hit enter to run the program - you should see "Hello World!" displayed on the screen.

When you type something and hit enter on the terminal you are running a command or program. From now on I will just say excute or run with a command.
