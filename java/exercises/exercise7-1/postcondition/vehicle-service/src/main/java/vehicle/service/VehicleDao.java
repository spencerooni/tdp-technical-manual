package vehicle.service;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import vehicle.api.Vehicle;

import java.util.List;

@RegisterMapper(VehicleDaoMapper.class)
public interface VehicleDao {
    @SqlQuery("sql/getVehicles")
    public List<Vehicle> getVehicles();
}