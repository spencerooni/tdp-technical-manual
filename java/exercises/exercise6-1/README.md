# Set up a client project
Create a new java project following the same patern as
we did for the `vehicle-service` this time call it `vehicle-client`.

Make sure it has it's own `build.gradle` file with the following dependency:
```
'io.dropwizard:dropwizard-client:0.9.2'
```
and an internal dependency on the `vehicle-api` project. Also ensure gradle
knows where to download the dropwizard dependency from. Finally make sure
that the `vehicle-client` is registered as a sub-project of the main `vehicle`
gradle project.

Create the following folder structure within the vehicle-client folder:

```
├── build.gradle
└── src
    └── main
        └── java
            └── vehicle
                └── client
                    └── VehicleClient.java
```

Make sure the `VehicleClient.java` file looks like the following:

```
package vehicle.client;

public class VehicleClient {

}
```

Make sure intellij builds the project correctly.
