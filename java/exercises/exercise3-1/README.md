#Intellij

Lets get out of the command line and use an IDE.

Update build.gradle, adding this line:

```
apply plugin: 'idea'
```

Now run the following command:

```
gradle idea
```

1. Open intelliJ (ctrl-space + type intellij)
2. Click `Import Project...`
3. Navigate to the directory where you created your `build.gradle` file
4. Click `Next`
5. Ensure `Create project from existing sources` is selected
6. Click `Next`
7. Use the default name and click `Next`
8. Select only the source set that ends with `main/java` and click `Next.
9. Click `Next` on the dependencies page
10. Select an SDK or click the `+` to add one.
    The default location of the JDK is `/Library/Java/JavaVirtualMachines/jdk1.8.0_XX.jdk/Contents/Home`
11. Click `Next` then `Finish`.

Lets now test out the application to see if the intellij has been configured correctly.

Run it:

1. Open the Project Explorer (ctrl-1) 
2. Right-click on `HelloWorld` and select `Run HelloWorld.main()`

This should cause the application to compile and run, outputting the vehicle at the bottom

1. Add another vehicle and use IntelliJ to compile and run the code.
