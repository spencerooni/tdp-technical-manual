Packages and Objects
--------------------

In java we can seperate out our code into folders in order to organised
large applications and remove any ambiguity between the origins of a class.

We do this using pacakges. We're going to now create a vehilce package which
will include everything we need to know about vehicles.

1. Create a directory called 'vehicle'
1. Create a directory in that folder called 'api'
3. Create a file in that directory called `Vehicle.java`

This should give you the following structure:

```
├── HelloWorld.class
├── HelloWorld.java
└── vehicle
    └── api
        └── Vehicle.java
```

Lets now add some properties to the vehicle object we've just created.

Fill the Vehicle.java file with the following:

```
package vehicle.api;

public class Vehicle {
    public int id;
    public String registration;
    public String colour;

    @Override
    public toString() {
        String vehicle = "{ id: " + id;
        vehicle += ", registration: " + registration;
        vehicle += ", colour: " + colour + "}";
        return vehice;
    }
}
```

Now open HelloWorld.java again. We are going to make some new vehicles and print them.

Make your HelloWorld.java file look like this:

```
import vehicle.api.Vehicle;

public class HelloWorld {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        System.out.println(vehicle);
    }
}
```

Now recompile and run the application.

```
javac HelloWorld.java
java HelloWorld
```
