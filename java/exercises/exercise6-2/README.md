# GET vehicles endpoint
## Create a Client

Update the Client to look like the following:

```
package vehicle.client;

import vehicle.api.Vehicle;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class VehicleClient {
    private Client httpClient;
    private String baseUrl;

    public VehicleClient(Client httpClient, String baseUrl) {
        this.httpClient = httpClient;
        this.baseUrl = baseUrl;
    }

    public List<Vehicle> getVehicles() {
        return httpClient.target(baseUrl)
            .path("vehicles")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .get(new GenericType<List<Vehicle>>() {});
    }
}
```
