# Gradle

Right now we have a relatively small application (DriverApp) but already it is
becoming a pain compile and run the application. We can do it through the command-line
but it would be hard to scale across many people especially if we were to add more
external libraries and make our code available to systems as a library.

These are things that we want to do and therefore we need a better way to manage
the build process and dependencies.

Can you think of something things that might be required to know about the build process
or dependent files:
  - Version
  - Location
  - Build steps
  - Class path
  - Build output location
  - Build number
  - Build publish location

## Organise
Gradle expects files to be in a certain place in order to use know that they are there and 
what they are to be used for. We can configure this but as our application grows we will 
want to enforce a nicer structure.

Our goal from here on will be to write a vehicle application. We will be able to use the
model object that we've created for this so we're going to forget about the DriverApp for now.

Can you make your working directory look like the following:
```
.
└─ src
   └── main
       └── java
           └── HelloWorld.java
           └── vehicle
               └── api
                   └── Vehicle.java
```

## Make gradle build your JAR
In your DriverApp directory create a file called `build.gradle`. In that enter the following line:
```
apply plugin: 'java'
```

Now run:
```
gradle build
```

That should run with no errors.

This is the equivalent of running javac followed by the jar command. 

Now lets try to run the JAR file:
```
java -jar build/libs/{name}.jar
```
We'll get an error which says: `no main manifest attribute, in build/libs/{name}.jar`

In order to fix this we're going to need a manifest. A manifest file allows us to do a
whole range of things but in particular it allows us set the entry point of our application.
Lets have a look at that now.

Update the `gradle.build` file as follows
```
apply plugin: 'java'

jar {
    manifest {
        attributes("Main-Class": "HelloWorld")
    }
}
```

This has created the JAR as an executable and when we run the java command again
it runs the original program.
