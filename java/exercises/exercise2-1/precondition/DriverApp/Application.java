import driver.api.Driver;
import vehicle.api.Vehicle;

public class Application {
    public static void main(String args[]) {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        Driver driver = new Driver();
        driver.vehicle = vehicle;
        driver.name = "Laura Smithson";
        driver.address = "10 Street, Town, Country";

        System.out.println(driver);
    }
}
