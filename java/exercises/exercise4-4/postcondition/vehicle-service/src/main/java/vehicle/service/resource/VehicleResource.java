package vehicle.service.resource;

import vehicle.api.Vehicle;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/vehicles")
@Produces(MediaType.APPLICATION_JSON)
public class VehicleResource {
    private List<Vehicle> vehicles;

    public VehicleResource() {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        this.vehicles = new ArrayList<>();
        this.vehicles.add(vehicle);
    }

    @GET
    public List<Vehicle> getVehicles() {
        return vehicles;
    }
}

