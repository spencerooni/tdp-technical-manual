# Import Dropwizard

We need to download the dropwizard library. Any ideas on how to do this?

## Add it to the gradle dependencies

Open the file `vehicle-service/build.gradle` and change it so it looks like:

```
apply plugin: 'java'

repositories {
    mavenCentral()
}

dependencies {
    compile (
        'io.dropwizard:dropwizard-core:0.9.2'
     )
}
```

This will import both the dropwizard library and the local API project that we've
created.

## Create Application Class
Using IntelliJ create a new Java Class in the `vehicle.service` package call this
`VehicleServiceApplication`.

This will be the entry point to our application and as such it should have a
main method. Make the Application class match below:

```
package vehicle.service;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import vehicle.service.resources.VehicleResource;

public class VehicleServiceApplication extends Application<Configuration> {
    public static void main(String[] args) throws Exception {
        new VehicleServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "vehicle-service";
    }

    @Override
    public void initialize(Bootstrap<Configuration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(Configuration configuration,
            Environment environment) {
        // nothing to do yet
    }

}
```

## Resolve dependencies
1. Open the gradle window`View > Tool Windows > Gradle`
2. Click the refresh button
