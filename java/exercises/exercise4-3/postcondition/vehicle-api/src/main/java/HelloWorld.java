import vehicle.api.Vehicle;

public class HelloWorld {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.id = 0;
        vehicle.registration = "ABC123";
        vehicle.colour = "Blue";

        Vehicle vehicle2 = new Vehicle();
        vehicle2.id = 1;
        vehicle2.registration = "ABC543";
        vehicle2.colour = "Blue";

        Vehicle vehicle3 = new Vehicle();
        vehicle3.id = 2;
        vehicle3.registration = "DEF123";
        vehicle3.colour = "Red";

        System.out.println(vehicle);
        System.out.println(vehicle2);
        System.out.println(vehicle3);
    }
}
