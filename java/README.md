# Prerequisites
This course assumes the participants are using Mac OSX and have run the [installation script](scripts/install-dependencies.sh).

# Day 1
## Introduction

- What do you know about Java?
- What do you wnat to know
    - Everyone to come up with 3 things and we'll discuss 3 objectives we want to achive as a class.
- Discuss schedule:

```
Day 1
1. Introduction to Microservices & reason for Java
2. Java basics
3. Gradle
4. IntelliJ
5. Dropwizard
6. Get Vehicles Endpoint

Day 2
1. Exploring JUnit
2. HTTP Client
3. Integration Test
4. Test Driven Development -- Replace with JDBC
5. Add Vehicle Endpoint

Day 3
1. Get Frontend Service -- Replace with AngularJS
2. Extend Frontend Service -- Replace with AngularJS
3. Performance Testing
4. Monitoring
5. Push to GitHub
6. Pipeline Demo
```

## Introduction to Microservices & reason for Java
This will be delivered by Will Hamill.

## Java Basics
- Minimum Viable Java [Exercise 1.1](exercises/exercise1-1/)
- Methods [Exercise 1.2](exercises/exercise1-2/)
- Packages [Exercise 1.3](exercises/exercise1-3/)
- External Libraries [Exercise 1.4](exercises/exercise1-4/)

## Gradle
- Introduction, build a JAR [Exercise 2.1](exercises/exercise2-1)

## IntelliJ
- Import and run the project in IntelliJ [Exercise 3.1](exercises/exercise3-1)

## Dropwizard
- Do some research into DropWizard. Split the group into 2. Give each subgroup a topic and
a few questions to answer, give them 15 minutes to prepare a 3 minute presentation. Allocated 15 mins
for presntations so in all the session should take 30 mins.
- Micro Presentations:
    1. HTTP
        - What is HTTP?
        - Why do we use it?
        - Examples of HTTP Requests & Respnoses
    2. REST
        - What is REST?
        - Why do we use it?
        - Examples?
- Create a new project in ItelliJ, configure for Gradle [Exercise 4.1](exercises/exercise4-1)
- Import DropWizard [Exercise 4.2](exercises/exercise4-2)
- Import our Vehicle API module [Exercise 4.3](exercises/exercise4-3)
- Add get vehicles endpoint to service [Exercise 4.4](exercises/exercise4-4)

# Day 2
## Add JDBC
- Add the required dependencies
- Add the DAO object
- Add a mapper
- Add the SQL file
- All done in [Exercise 7.1](exercises/exercise7-1)

## Exploring JUnit
- Crossword on all things Java [crossword](resources/day2-crossword.pdf)
- What is unit testing?
- What specific things should we test about this method?
    1. That 1 vehicle is returned
    2. That the vehicle returned is the one we expect
    3. That the vehicle list is transformed into JSON
- Write the tests [Exercise 5.1](exercises/exercise5-1)

## Create a client
- Create a new gradle project [Exercise 6.1](exercises/exercise6-1)
- Create a method in the client to make a Java interface for getVehicles [Exercise 6.2](exercises/exercise6-2)

## Integration Test
- Write an integration test to ensure that the service behaves as expected [Exercise 6.3](exercises/exercise6-3)

## Add Vehicle Endpoint
- What tests would you exepect to find for Create Vehicle?
   1. When vehicle is created another vehicle is returned from create vehicle
- Write a test to cover that scenario and then write the code
- The current setup isn't that nice - lets refactor and add a data layer
- Refactor tests - Mocking.
- Extend the get vehicle resource tests:
    1. When there is more than one vehicle, multiple vehicles are returned
    2. When there are no vehicles an empty list is returned
    3. When the vehicle has a null property what happens?
- Extend client and integration tests

# Day 3
- Discuss what we covered last two days, look at "What I want to learn" sheet

## Get Frontend Service
- Download frontend service [by cloning this URL]() - requires Git

## Extend Frontend Service
- Add the client as a dependency [Exercise X.X](exercises/exerciseX-X)
- Display vehicles [Exercise X.X](exercises/exerciseX-X)
- Create a vehicle form [Exercise X.X](exercises/exerciseX-X)
- Handle POST vehicle [Exercise X.X](exercises/exerciseX-X)

## Performance Testing
- Clone JMeter Practical from GIT
- Write a JMeter script to get a vehicle from your service
- Write a JMeter script to add a vehicle to your service (optional)

## Monitoring
- Add timed annotations to resource methods [Exercise X.X](exercises/exerciseX-X)
- Graph the metrics using graphinte [Exercise X.X](exercises/exerciseX-X)
- Re-run performance tests and see output on graphite

## Github
- Everyone create github profiles, add public keys and push up what we've done so far [Exercise X.X](exercises/exerciseX-X)

## Show the pipeline of jenkins running
- Push up a change to the master banch of the vehicle-service and go have a look at Jenkins
- Explain how the build is run, tests are run and developers are notified in case of change
- Show how the changes can be tracked and any breaking commit identified
