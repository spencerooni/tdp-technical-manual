package com.kainos.foundations.subscribers.controllers;

import io.dropwizard.views.View;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.kainos.foundations.subscribers.views.Index;

@Path("/")
public class SubscribersController {

	@GET
	@Path("index")
	@Produces(MediaType.TEXT_HTML)
	public View index() {
		return new Index();
	}
	
	@POST
	@Path("addSubscriber")
	public void addSubscriber(@FormParam("name") String name, @FormParam("email") String email) {
		System.out.println("Name: " + name);
		System.out.println("Email: " + email);
	}
	
}
