package com.kainos.foundations.subscribers;

import com.google.common.collect.ImmutableMap;
import com.kainos.foundations.subscribers.config.SubscribersConfiguration;

import io.dropwizard.Application;
import io.dropwizard.setup.*;
import io.dropwizard.views.ViewBundle;

public class SubscribersApplication extends Application<SubscribersConfiguration> {

	public static void main(String[] args) throws Exception {
		new SubscribersApplication().run(args);
	}

	@Override
    public void initialize(Bootstrap<SubscribersConfiguration> bootstrap) {        
        bootstrap.addBundle(new ViewBundle<SubscribersConfiguration>() {
	        @Override
	        public ImmutableMap<String, ImmutableMap<String, String>> getViewConfiguration(SubscribersConfiguration config) {
	            return config.getViewRendererConfiguration();
	        }
        });               
    }
	
	@Override
	public void run(SubscribersConfiguration configuration, Environment environment)
			throws Exception {
		/* final ViewsResource viewsResource = new ViewsResource();
		environment.jersey().register(viewsResource); */
	}

}
