package com.kainos.foundations.subscribers;

import com.google.common.collect.ImmutableMap;
import com.kainos.foundations.subscribers.config.SubscribersConfiguration;
import com.kainos.foundations.subscribers.controllers.SubscribersController;

import io.dropwizard.Application;
import io.dropwizard.setup.*;
import io.dropwizard.views.ViewBundle;
import io.dropwizard.assets.*;

public class SubscribersApplication extends Application<SubscribersConfiguration> {

	public static void main(String[] args) throws Exception {
		new SubscribersApplication().run(args);
	}

	@Override
    public void initialize(Bootstrap<SubscribersConfiguration> bootstrap) {        
        bootstrap.addBundle(new ViewBundle<SubscribersConfiguration>() {
	        @Override
	        public ImmutableMap<String, ImmutableMap<String, String>> getViewConfiguration(SubscribersConfiguration config) {
	            return config.getViewRendererConfiguration();
	        }
        });   
    }
	
	@Override
	public void run(SubscribersConfiguration configuration, Environment environment)
			throws Exception {
		SubscribersController subscribersController = new SubscribersController();
		environment.jersey().register(subscribersController);
	}

}
