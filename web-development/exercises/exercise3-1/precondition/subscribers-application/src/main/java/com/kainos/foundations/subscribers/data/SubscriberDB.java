package com.kainos.foundations.subscribers.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.kainos.foundations.subscribers.model.Subscriber;

public class SubscriberDB {
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/SubscribersDB";
	
	private static final String USERNAME = "subscribers_app";
	private static final String PASSWORD = "subscribers_pwd";
	
	private Connection connection;
	
	public SubscriberDB() {
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println("Error loading JDBC driver: ");
			e.printStackTrace();
		}
	}
	
	public List<Subscriber> getSubscribers() {
		
		List<Subscriber> subscribers = new ArrayList<Subscriber>();
		
		try {
			connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			Statement statement = connection.createStatement();
			String SQL = "SELECT name, email FROM subscribers";
			ResultSet results = statement.executeQuery(SQL);
		
			while(results.next()) {
				String name = results.getString("name");
				String email = results.getString("email");
				Subscriber subscriber = new Subscriber(name, email);
				subscribers.add(subscriber);
			}
			
			results.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			System.out.println("Error executing getSubscribers statement:");
			e.printStackTrace();
		}
		
		return subscribers;
	}
	
	public void insertSubscriber(String name, String email) {
		
		try {
			connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
			String SQL = "INSERT INTO subscribers (name, email) VALUES (?, ?)";
			PreparedStatement statement = connection.prepareStatement(SQL);
			statement.setString(1, name);
			statement.setString(2, email);
			
			statement.executeUpdate();
			
			statement.close();
			connection.close();
			
		} catch (Exception e) {
			System.out.println("Error executing insert of subscriber: ");
			e.printStackTrace();
		}
	}

}
