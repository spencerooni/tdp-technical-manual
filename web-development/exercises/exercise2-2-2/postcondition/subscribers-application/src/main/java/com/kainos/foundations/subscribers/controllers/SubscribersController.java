package com.kainos.foundations.subscribers.controllers;

import io.dropwizard.views.View;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.kainos.foundations.subscribers.data.SubscriberDB;
import com.kainos.foundations.subscribers.model.Subscriber;
import com.kainos.foundations.subscribers.views.Index;

@Path("/")
public class SubscribersController {
	
	private SubscriberDB subscriberDb;

	public SubscribersController() {
		subscriberDb = new SubscriberDB();
	}
	
	@GET
	@Path("index")
	@Produces(MediaType.TEXT_HTML)
	public View index() {
		List<Subscriber> subscribers = subscriberDb.getSubscribers();
		return new Index(subscribers);
	}
	
	@POST
	@Path("addSubscriber")
	public Object addSubscriber(@FormParam("name") String name, @FormParam("email") String email) {
		subscriberDb.insertSubscriber(name, email);
		List<Subscriber> subscribers = subscriberDb.getSubscribers();
		return new Index(subscribers);
	}
}
